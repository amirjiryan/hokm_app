/* eslint-disable prettier/prettier */
import React, { useRef } from "react";
import { Animated, PanResponder } from "react-native";
const Draggable = (props) => {
  const pan = useRef(new Animated.Value(0)).current;
  const panResponder = useRef(
    PanResponder.create({
      onMoveShouldSetPanResponder: () => true,

      onPanResponderMove: Animated.event([null, { dy: pan }],{useNativeDriver: false}),
      onPanResponderRelease: () => {
        props.panRelease(pan);
      },
    })
  ).current;
  return (
    <Animated.View // Special animatable View
      style={{
        transform: [{ translateX: 0 }, { translateY: pan }],
      }}
      {...panResponder.panHandlers}
    >
      {props.children}
    </Animated.View>
  );
};
export default Draggable;
