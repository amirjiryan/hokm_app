import React from 'react';
import usePlayable from '../../custom-hooks/usePlayable'
import Suit from './suit'
import { View } from 'react-native';
import Draggable from './Draggable'
const Playablesuit = ({ suit }) => {
    const [handleDrag] = usePlayable("hokm", [suit]);
    return (
        <Draggable panRelease={handleDrag}>
            <Suit suit={suit} />
        </Draggable>
    );
}

export default Playablesuit;