import React, { useState, useEffect, memo } from 'react';
import Socket from "../../socket/index";
import ReciveableCard from '../cards/reciveableCard';
import { View, StyleSheet } from 'react-native'
const Deck = () => {
    const [cardPlayed, setCardPlayed] = useState([]);
    if (cardPlayed.length === 4) {
        setTimeout(() => {
            setCardPlayed([])
        }, 2000);
    }
    useEffect(() => {
        Socket.on("card-played", function (card, name) {
            setCardPlayed(prevCardsPlayed => [...prevCardsPlayed, { card, name }]);
        });
    }, []);
    return (
        <View style={styles.deck}>
            {cardPlayed.map(card => <ReciveableCard card={card.card} name={card.name} />)}
        </View>


    );
}
const styles = StyleSheet.create({
    deck: {
        flex: 1,
        justifyContent: "center",
        overflow: "visible",
        width: "100%",
        height: "100%",
        position: "absolute"

    }
});
export default Deck;