import React, { useState, useEffect } from 'react';
import Socket from "../../socket/index";
import Room from './room';
import { Text, FlatList } from 'react-native'
const RoomsList = () => {
    const [rooms, setRooms] = useState([]);
    useEffect(() => {
        Socket.emit("reqListOfGames");
        Socket.on("new-game", function (gameName) {
            setRooms(prev => [...prev, gameName]);
        });
        Socket.on("listOfGames", function (games) {
            setRooms(games);
            console.log(games);
        })
    }, []);

    return (

        <FlatList
            data={rooms}
            renderItem={({ item, index }) => <Room room={item} />}
            keyExtractor={item => item + "name"}

        />

    )
}

export default RoomsList;