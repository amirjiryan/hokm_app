import React from "react";
import Card from './card';
import usePos from '../../custom-hooks/usePos'
import { StyleSheet, View } from 'react-native'
const ReciveableCard = ({ card, name }) => {
    const Pos = usePos(name);
    return (

        <Card card={card} style={styles[Pos]}></Card>

    );
}
const styles = StyleSheet.create({
    teamate: {
        position: 'absolute',
        transform: [{ translateX: -100 }],
        top: 0,
        zIndex: 100000,
        paddingTop: 20,
        marginLeft: 60,
        left: "50%",

    },
    first: {
        backgroundColor: "red",
        position: 'absolute',
        right: 40,
        top: '50%',
        transform: [{ translateY: -60 }]

    },
    second: {
        backgroundColor: "yellow",
        position: 'absolute',
        left: 40,
        top: '50%',
        transform: [{ translateY: -60 }]

    },
    me: {
        position: 'absolute',
        transform: [{ translateX: -100 }],
        bottom: 0,
        zIndex: 100000,
        paddingBottom: 160,
        marginLeft: 60,
        left: "50%",

    }
})
// controlled component , composable component , socket.emit callbackfunction
export default ReciveableCard;